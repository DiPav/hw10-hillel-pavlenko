package cofee.order;

import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CoffeeOrderBoard {
    public static final String START_MESSAGE_FORMAT = "===========%n# / Name%n";
    public static final String GRID_MESSAGE_FORMAT = "%d / %s%n";
    Map<Integer, String> orders = new ConcurrentHashMap<>();
    int runningNumber = 1;

    public void add(@NotNull Order order) {
        orders.put(runningNumber++, order.getName());
    }

    public void deliver() {
        int min = findFirstInQueue();
        orders.remove(min);
    }

    public void deliver(int numberOfOrder) {
        orders.remove(numberOfOrder);
    }

    public void draw() {
        System.out.printf(START_MESSAGE_FORMAT);
        for (Map.Entry<Integer, String> maps : orders.entrySet()) {
            System.out.printf(GRID_MESSAGE_FORMAT, maps.getKey(), maps.getValue());
        }
    }

    private int findFirstInQueue() {
        int res = 0;
        for (int i = 1; i < orders.size(); i++) {
            if (orders.get(i) != null) {
                res = i;
                break;
            }
        }
        return res;
    }
}
